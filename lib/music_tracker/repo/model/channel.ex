# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.Repo.Model.Channel do
  use MusicTracker.Schema
  alias MusicTracker.Repo.Model.PlayLog

  @type t :: %__MODULE__{
          name: String.t(),
          homepage: String.t(),
          logo_url: String.t(),
          tracking_activated: boolean(),
          poll_worker: string,
          poll_args: map(),
          extract_worker: string,
          extract_args: map(),
          save_worker: string,
          save_args: map()
        }

  schema "channel" do
    field(:name, :string)
    field :homepage, :string
    field :logo_url, :string
    field :tracking_activated, :boolean
    field :poll_worker, :string
    field :poll_args, :map
    field :extract_worker, :string
    field :extract_args, :map
    field :save_worker, :string
    field :save_args, :map
    has_many(:play_logs, PlayLog)
    timestamps()
  end

  def changeset(channel, attrs \\ %{}) do
    channel
    |> cast(attrs, [
      :name,
      :homepage,
      :logo_url,
      :tracking_activated,
      :poll_worker,
      :poll_args,
      :extract_worker,
      :extract_args,
      :save_worker,
      :save_args
    ])
    |> validate_required([:name])
    |> unique_constraint([:name])
  end
end
