# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.Repo.Channels do
  import Ecto.Query
  alias MusicTracker.Repo
  alias MusicTracker.Repo.Model.Channel

  @doc """
  Get a radio channel by its name
  """
  @spec get_by_name(name :: String.t()) :: Channel.t() | nil
  def get_by_name(name) do
    Repo.get_by(Channel, name: name)
  end

  @doc """
  Get a radio channel by its ID
  """
  def get_by_id(id) do
    Repo.get(Channel, id)
  end

  @doc """
  Insert new radio channel
  """
  @spec insert_channel(map) ::
          {:ok, Channel.t()} | {:error, Changeset.t()}
  def insert_channel(params) do
    %Channel{}
    |> Channel.changeset(params)
    |> Repo.insert()
  end

  def count_all_channels() do
    from(s in Channel, select: count()) |> Repo.one()
  end

  def get_tracked_channels() do
    from(c in Channel, where: c.tracking_activated == true, select: c) |> Repo.all()
  end
end
