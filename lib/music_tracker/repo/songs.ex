# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.Repo.Songs do
  import Ecto.Query
  alias MusicTracker.Repo
  alias MusicTracker.Repo.Model.Song
  alias MusicTracker.Repo.Model.SongPlayHistory
  alias MusicTracker.Repo.Model.SongMedia
  alias Ecto.Changeset
  alias Phoenix.PubSub
  require Logger

  @pubsub MusicTracker.PubSub

  @doc """
  Add a new play log entry to an existing channel
  """
  @spec insert_song(attrs :: map()) ::
          {:ok, Song.t()} | {:error, Changeset.t()}
  def insert_song(attrs \\ %{}) do
    %Song{} |> Song.create_changeset(attrs) |> Repo.insert()
  end

  @spec get_song_by_analyser_id(analyzer :: atom(), id :: String.t()) :: Song.t() | nil
  def get_song_by_analyser_id(analyzer, id) do
    query =
      from(s in Song,
        join: sai in assoc(s, :analyzer_ids),
        where: sai.analyzer_name == ^analyzer,
        where: sai.analyzer_song_uuid == ^id,
        select: s
      )

    Repo.one(query)
  end

  @spec get_song_by_similarity(title :: String.t(), album :: String.t(), artists :: [String.t()]) ::
          {:ok, Song.t() | nil} | {:no_unique_similarity, String.t()}
  def get_song_by_similarity(title, album, artists) do
    query =
      ~s|SELECT s.id FROM song s JOIN song_artist sa ON s.id = sa.song_id
    CROSS JOIN unnest(array[$1]) a
    WHERE similarity(s.album , $3) * similarity(s.title , $2) * similarity(sa.name, a) > 0.6
    ORDER BY similarity(s.album , $3) * similarity(s.title , $2) * similarity(sa.name, a) DESC LIMIT 1|

    artists_array = artists |> Enum.map(fn artist -> "'#{artist}'" end) |> Enum.join(",")
    result = Repo.query!(query, [artists_array, title, album])

    case result.num_rows do
      0 -> {:ok, nil}
      _ -> {:ok, Repo.load(Song, {result.columns, List.first(result.rows)})}
    end
  end

  @spec get_song_by_analyser_id_or_similarity(
          {analyzer :: atom(), id :: String.t()},
          title :: String.t(),
          album :: String.t(),
          artists :: [String.t()]
        ) ::
          {:ok, Song.t() | nil} | {:no_unique_similarity, String.t()}
  def get_song_by_analyser_id_or_similarity({analyzer, id}, title, album, artists) do
    case get_song_by_analyser_id(analyzer, id) do
      nil ->
        # Song not found by analyzer id
        case get_song_by_similarity(title, album, artists) do
          {:ok, song} when not is_nil(song) ->
            # Got song by title similatity
            # Associate with analyzer id so we'll find it directly next time
            add_song_analyzer_id(song, analyzer, id)
            {:ok, song}

          other ->
            other
        end

      song ->
        # Song found by analyzer id
        {:ok, song}
    end
  end

  def add_song_analyzer_id(song, analyszer_name, analyzer_id) do
    Ecto.build_assoc(song, :analyzer_ids, %{
      analyzer_name: analyszer_name,
      analyzer_song_uuid: analyzer_id
    })
    |> Repo.insert()
  end

  def add_song_play_history(song, hash, channel, start_time, end_time) do
    case Repo.get_by(SongPlayHistory,
           channel_id: channel.id,
           song_id: song.id,
           start_time: start_time,
           end_time: end_time
         ) do
      nil ->
        %SongPlayHistory{}
        |> SongPlayHistory.create_changeset(song, channel, %{
          start_time: start_time,
          end_time: end_time,
          extract_hash: hash
        })
        |> Repo.insert()

      song_play_history ->
        {:ok, song_play_history}
    end
  end

  def add_song_media(song, type, payload) do
    case Repo.get_by(SongMedia, type: type, song_id: song.id, payload: payload) do
      nil ->
        Ecto.build_assoc(song, :medias, %{
          type: type,
          payload: payload
        })
        |> Repo.insert()

      media ->
        media |> Changeset.change(payload: Map.merge(media.payload, payload)) |> Repo.update()
    end
  end

  @doc """
  Add an artist name to a song. The artist name is added only if no artist exists with similarity > 0.6
  """
  def add_song_artist(song, name) do
    %{rows: [[count]], num_rows: _} =
      Repo.query!(
        ~s|SELECT count(*) FROM song_artist WHERE song_id=$1 and similarity(name, $2) > 0.6|,
        [Ecto.UUID.dump!(song.id), name]
      )

    case count do
      0 ->
        Ecto.build_assoc(song, :artists, %{name: name})
        |> Repo.insert()

      _ ->
        nil
    end
  end

  @doc """
  Add an musical kind name to a song. The musical kind name is added only if no artist exists with similarity > 0.6
  """
  def add_song_musical_kind(song, name) do
    %{rows: [[count]], num_rows: _} =
      Repo.query!(
        ~s|SELECT count(*) FROM musical_kind WHERE song_id=$1 and similarity(name, $2) > 0.6|,
        [Ecto.UUID.dump!(song.id), name]
      )

    case count do
      0 ->
        Ecto.build_assoc(song, :musical_kinds, %{name: name})
        |> Repo.insert()

      _ ->
        nil
    end
  end

  @spec update_song_fields(song :: Song.t(), attrs :: map) :: Song.t()
  def update_song_fields(song, attrs \\ %{}) do
    song
    |> Changeset.change()
    |> Changeset.change(title: Map.get(attrs, "title", song.title))
    |> Changeset.change(album: Map.get(attrs, "album", song.album))
    |> Changeset.change(label: Map.get(attrs, "label", song.label))
    |> Changeset.change(year: Map.get(attrs, "year", song.year))
    |> Repo.update!()
  end

  def exists_song_play_history_with_hash?(hash) do
    from(ph in SongPlayHistory, where: ph.extract_hash == ^hash)
    |> Repo.exists?()
  end

  def count_all_songs() do
    from(s in Song, select: count()) |> Repo.one()
  end

  def count_all_play_history() do
    from(s in SongPlayHistory, select: count()) |> Repo.one()
  end

  def recent_play_history(limit \\ 10) do
    from(ph in SongPlayHistory,
      join: s in assoc(ph, :song),
      join: c in assoc(ph, :channel),
      left_join: m in assoc(s, :medias),
      preload: [song: {s, medias: m}, channel: c],
      select: ph,
      order_by: [desc: ph.start_time]
    )
    |> Repo.all()
    |> Enum.take(limit)
  end

  def broadcast_song_analyzed(channel, song) do
    Logger.info("[#{channel.name}] broadcasting :song_analyzed for song '#{song.title}'")
    PubSub.broadcast(@pubsub, "trackers", {:song_analyzed, %{song_id: song.id}})
  end

  def subscribe_trackers_activity() do
    PubSub.subscribe(@pubsub, "trackers")
  end
end
