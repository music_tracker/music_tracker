defmodule MusicTracker.Repo do
  use Ecto.Repo,
    otp_app: :music_tracker,
    adapter: Ecto.Adapters.Postgres
end
