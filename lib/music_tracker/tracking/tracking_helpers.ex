# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
defmodule MusicTracker.Tracking.TrackingHelpers do
  alias MusicTracker.Repo.Channels

  @spec hash_payload(payload :: map()) :: String.t()
  def hash_payload(payload) do
    hash_source = payload |> Jason.encode!()
    :crypto.hash(:sha256, hash_source) |> Base.encode64()
  end

  def get_poll_result(context) do
    get_result(context, "poll")
  end

  def get_extract_result(context) do
    get_result(context, "extract")
  end

  def get_result(context, job) do
    case context do
      %{^job => %{"result" => result}} -> {:ok, result}
      _ -> :error
    end
  end

  def get_channel(context) do
    with %{"__channel_id__" => channel_id} <- context,
         channel when not is_nil(channel) <- Channels.get_by_id(channel_id) do
      {:ok, channel}
    else
      _ ->
        :error
    end
  end
end
