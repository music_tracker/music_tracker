# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
defmodule MusicTracker.Tracking.Process do
  require Logger

  @moduledoc """
  Process job order:
   - poll
   - extract
   - save
   - analyze
  """

  @default_schedule_delay 60
  @schedule_key "__schedule_in__"
  @channel_id_key "__channel_id__"

  def init_processes() do
    # Make sure there are no waiting job
    Oban.cancel_all_jobs(Oban.Job)

    for channel <- MusicTracker.Repo.Channels.get_tracked_channels() do
      Logger.info("[#{channel.name}] Starting tracking process")
      MusicTracker.Tracking.Process.launch_process(channel)
    end
  end

  def launch_process(channel, schedule_in \\ @default_schedule_delay) do
    %{
      @channel_id_key => channel.id,
      @schedule_key => schedule_in,
      "poll" => %{"worker" => channel.poll_worker, "args" => channel.poll_args},
      "extract" => %{"worker" => channel.extract_worker, "args" => channel.extract_args},
      "save" => %{"worker" => channel.save_worker, "args" => channel.save_args}
    }
    |> run_job("poll", schedule_in)
  end

  def continue_process(job_result, context) do
    %{@channel_id_key => channel_id} = context
    channel = MusicTracker.Repo.Channels.get_by_id(channel_id)

    case job_result do
      {:ok, job, result} ->
        new_context = store_result_in_context(context, job, result)
        next_job = get_next_job(job)

        if not is_nil(next_job) do
          run_job(new_context, next_job)
        else
          Logger.debug("[#{channel.name}] process ended with context: #{inspect(new_context)}")
          schedule_process(new_context)
        end

      {:error_continue, job, cause} ->
        new_context = store_error_in_context(context, job, cause)
        next_job = get_next_job(job)

        if not is_nil(next_job) do
          run_job(new_context, next_job)
        else
          Logger.debug("[#{channel.name}] process ended with context: #{inspect(new_context)}")
          schedule_process(new_context)
        end

      {:error_stop, job, cause} ->
        new_context = store_error_in_context(context, job, cause)
        Logger.debug("[#{channel.name}] process ended with context: #{inspect(new_context)}")
        schedule_process(new_context)
    end

    :ok
  end

  defp schedule_process(context) do
    %{@channel_id_key => channel_id, @schedule_key => schedule_in} = context
    channel = MusicTracker.Repo.Channels.get_by_id(channel_id)
    Logger.info("[#{channel.name}] Re-scheduling process execution in #{schedule_in} seconds.")
    launch_process(channel, schedule_in)
  end

  defp run_job(context, job_name, schedule_in \\ 0) do
    job_context = Map.get(context, job_name)

    with %{"worker" => worker, "args" => args} <- job_context,
         {:ok, worker} <- Oban.Worker.from_string(worker) do
      %{"args" => args, "context" => context}
      |> worker.new(schedule_in: schedule_in)
      |> Oban.insert()
    else
      error ->
        Logger.error("Can't run #{job_name} job (#{error}). Invalid context: #{inspect(context)}")
    end
  end

  defp store_result_in_context(context, job, result) do
    store_in_context(context, job, "result", result)
  end

  defp store_error_in_context(context, job, error) do
    store_in_context(context, job, "error", error)
  end

  defp store_in_context(context, job, key, value) do
    job_context = Map.get(context, job) |> Map.put(key, value)
    Map.put(context, job, job_context)
  end

  defp get_next_job("poll"), do: "extract"
  defp get_next_job("extract"), do: "save"
  # "analyze"
  defp get_next_job("save"), do: nil
  defp get_next_job("analyze"), do: nil
end
