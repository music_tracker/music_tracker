# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
defmodule MusicTracker.Tracking.RPSaveWorker do
  alias MusicTracker.Tracking.TrackingHelpers
  alias MusicTracker.Repo.Songs
  use Oban.Worker
  require Logger

  @job "save"
  @analyzer_id "RPAnalyzer"
  @rp_img_url "https://img.radioparadise.com/"

  def perform(%{args: %{"args" => _args, "context" => context}}) do
    with {:ok, %{"hash" => hash, "payload" => payload}} <-
           TrackingHelpers.get_extract_result(context),
         {:ok, channel} <- TrackingHelpers.get_channel(context) do
      if Songs.exists_song_play_history_with_hash?(hash) do
        MusicTracker.Tracking.Process.continue_process(
          {:error_stop, @job, "Extracted payload already processed"},
          context
        )
      else
        song =
          case Songs.get_song_by_analyser_id_or_similarity(
                 {@analyzer_id, payload["song_id"]},
                 payload["title"],
                 payload["album"],
                 [payload["artist"]]
               ) do
            {:ok, nil} ->
              # Song not found, create a new one
              Logger.debug("Adding new song '#{payload["title"]}'")

              {:ok, song} =
                %{
                  "title" =>
                    Map.get(payload, "title", "") |> clean_field() |> String.capitalize(),
                  "album" =>
                    Map.get(payload, "album", "") |> clean_field() |> String.capitalize(),
                  "year" => Map.get(payload, "year") |> String.to_integer()
                }
                |> Songs.insert_song()

              Songs.add_song_analyzer_id(song, @analyzer_id, Map.get(payload, "song_id"))
              song

            {:ok, song} ->
              Logger.debug("Updating existing song")

              song
              |> Songs.update_song_fields(%{
                payload
                | "year" => Map.get(payload, "year") |> String.to_integer()
              })

            {:no_unique_similarity, query} ->
              Logger.warn(
                "One or more query match similarity criterias, title='#{payload["title"]}', artists=#{payload["artist"]}"
              )

              Logger.warn("Lear to following query to learn more: #{query}")

              nil
          end

        # Add song artist
        Songs.add_song_artist(song, String.trim(Map.get(payload, "title", "")))

        # Add musical history
        sched_time = Map.get(payload, "sched_time") |> String.to_integer()
        start_time = sched_time |> DateTime.from_unix!()
        duration = Map.get(payload, "duration") |> String.to_integer()
        end_time = DateTime.add(start_time, div(duration, 1000))

        Logger.debug("New '#{song.title}' play history at #{start_time}")

        Songs.add_song_play_history(song, hash, channel, start_time, end_time)

        # Add media
        # IO.inspect(payload)
        covers =
          %{}
          |> Map.put("cover", @rp_img_url <> Map.get(payload, "cover", ""))
          |> Map.put("cover_med", @rp_img_url <> Map.get(payload, "cover_med", ""))
          |> Map.put("cover_small", @rp_img_url <> Map.get(payload, "cover_small", ""))
          |> Map.filter(fn {_, v} -> v != @rp_img_url end)

        Songs.add_song_media(song, "covers", covers)
        Songs.broadcast_song_analyzed(channel, song)

        MusicTracker.Tracking.Process.continue_process({:ok, @job, song}, context)
      end
    else
      :error ->
        MusicTracker.Tracking.Process.continue_process(
          {:error_stop, @job, "Invalid context: Poll result not found"},
          context
        )
    end
  end

  defp clean_field(field) do
    case field do
      nil -> ""
      other -> other
    end
    |> String.trim()
  end
end
