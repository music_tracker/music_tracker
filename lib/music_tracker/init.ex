defmodule MusicTracker.Init do
  alias MusicTracker.Repo.Channels

  def seed() do
    api_get_poll_worker = Oban.Worker.to_string(MusicTracker.Tracking.APIGetPollWorker)

    channels = [
      %{
        name: "FIP",
        homepage: "https://www.fip.fr/",
        poll_worker: api_get_poll_worker,
        poll_args: %{"url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip"},
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "FIP Rock",
        homepage: "https://www.fip.fr/rock/webradio",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip_rock"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "FIP Jazz",
        homepage: "https://www.fip.fr/jazz/webradio",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip_jazz"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "FIP Groove",
        homepage: "https://www.fip.fr/groove/webradio",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip_groove"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "FIP POP",
        homepage: "https://www.fip.fr/pop/webradio",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip_pop"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "FIP Electro",
        homepage: "https://www.fip.fr/electro/webradio",
        poll_worker: api_get_poll_worker,
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip_electro"
        },
        tracking_activated: true
      },
      %{
        name: "FIP Monde",
        homepage: "https://www.fip.fr/musiques-du-monde/webradio",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip_world"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "FIP Reggae",
        homepage: "https://www.fip.fr/reggae/webradio",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip_reggae"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "FIP Tout nouveau",
        homepage: "https://www.fip.fr/tout-nouveau-tout-fip/webradio",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/fip/webradios/fip_nouveautes"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Radio Paradise Main Mix",
        homepage: "https://radioparadise.com/",
        poll_worker: api_get_poll_worker,
        poll_args: %{"url" => rp_url("0")},
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RPExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RPSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Radio Paradise Mellow Mix",
        homepage: "https://radioparadise.com/",
        poll_worker: api_get_poll_worker,
        poll_args: %{"url" => rp_url("1")},
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RPExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RPSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Radio Paradise Rock Mix",
        homepage: "https://radioparadise.com/",
        poll_worker: api_get_poll_worker,
        poll_args: %{"url" => rp_url("2")},
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RPExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RPSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Radio Paradise World/Etc Mix",
        homepage: "https://radioparadise.com/",
        poll_worker: api_get_poll_worker,
        poll_args: %{"url" => rp_url("3")},
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RPExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RPSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Mouv' Rap Français",
        homepage: "https://www.radiofrance.fr/mouv",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/mouv/webradios/mouv_rapfr"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Mouv' Classics",
        homepage: "https://www.radiofrance.fr/mouv",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/mouv/webradios/mouv_classics"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Mouv' Kids 'n Family",
        homepage: "https://www.radiofrance.fr/mouv",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" =>
            "https://www.radiofrance.fr/api/v1.7/stations/mouv/webradios/mouv_kids_n_family"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Mouv' Rap US",
        homepage: "https://www.radiofrance.fr/mouv",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/mouv/webradios/mouv_rapus"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Mouv' RnB & Soul",
        homepage: "https://www.radiofrance.fr/mouv",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/mouv/webradios/mouv_rnb"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      },
      %{
        name: "Mouv' DanceHall",
        homepage: "https://www.radiofrance.fr/mouv",
        poll_worker: api_get_poll_worker,
        poll_args: %{
          "url" => "https://www.radiofrance.fr/api/v1.7/stations/mouv/webradios/mouv_dancehall"
        },
        extract_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceExtractWorker),
        save_worker: Oban.Worker.to_string(MusicTracker.Tracking.RadioFranceSaveWorker),
        tracking_activated: true
      }
    ]

    Enum.each(channels, fn %{name: name} = channel ->
      if is_nil(Channels.get_by_name(name)) do
        {:ok, _} = Channels.insert_channel(channel)
      end
    end)
  end

  defp fip_url(station_id),
    do:
      String.replace(
        ~s|https://www.fip.fr/latest/api/graphql?operationName=Now&variables={"stationId"%3A__ID__}&extensions={"persistedQuery"%3A{"version"%3A1%2C"sha256Hash"%3A"95ed3dd1212114e94d459439bd60390b4d6f9e37b38baf8fd9653328ceb3b86b"}}|,
        "__ID__",
        station_id
      )

  defp rp_url(station_id),
    do:
      String.replace(
        ~s|https://api.radioparadise.com/api/nowplaying_list?chan=__ID__|,
        "__ID__",
        station_id
      )
end
