defmodule MusicTrackerWeb.LayoutView do
  use MusicTrackerWeb, :view

  # Phoenix LiveDashboard is available only in development by default,
  # so we instruct Elixir to not warn if the dashboard route is missing.
  @compile {:no_warn_undefined, {Routes, :live_dashboard_path, 2}}

  alias MusicTrackerWeb.Endpoint
  import MusicTracker.Gettext

  def sidebar_nav_link(assigns) do
    ~H"""
    <.link
      navigate={@to}
      class={
        "text-gray-700 hover:text-gray-900 group flex items-center px-2 py-2 text-sm font-medium rounded-md #{if @active == true, do: "bg-gray-200", else: "hover:bg-gray-50"}"
      }
      aria-current={if @active == true, do: "true", else: "false"}
    >
      <%= render_slot(@inner_block) %>
    </.link>
    """
  end

  def account_dropdown(assigns) do
    ~H"""
    <button
      type="button"
      class="max-w-xs bg-white rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-500 lg:p-2 lg:rounded-md lg:hover:bg-gray-50"
      id={@id}
      aria-expanded="false"
      aria-haspopup="true"
      phx-click={show_dropdown("##{@id}-dropdown")}
    >
      <img class="h-8 w-8 rounded-full" src={@current_user.avatar_url} alt="" />
      <span class="hidden ml-3 text-gray-700 text-sm font-medium lg:block">
        <span class="sr-only">Open user menu for</span><%= @current_user.name %>
      </span>
      <Heroicons.LiveView.icon
        name="chevron-down"
        type="solid"
        class="hidden flex-shrink-0 ml-1 h-5 w-5 text-gray-400 lg:block"
      />
    </button>
    <div
      class="hidden origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
      role="menu"
      aria-orientation="vertical"
      aria-labelledby="user-menu-button"
      tabindex="-1"
      id={"#{@id}-dropdown"}
      phx-click-away={hide_dropdown("##{@id}-dropdown")}
    >
      <!-- Active: "bg-gray-100", Not Active: "" -->
      <a
        href="#"
        class="block px-4 py-2 text-sm text-gray-700"
        role="menuitem"
        tabindex="-1"
        id="user-menu-item-0"
      >
        Profile
      </a>
      <a
        href="#"
        class="block px-4 py-2 text-sm text-gray-700"
        role="menuitem"
        tabindex="-1"
        id="user-menu-item-1"
      >
        Settings
      </a>
      <.link
        href={Routes.o_auth_callback_path(Endpoint, :sign_out)}
        method={:delete}
        class="block px-4 py-2 text-sm text-gray-700"
        role="menuitem"
        tabindex="-1"
        id="user-menu-item-2"
      >
        Logout
      </.link>
    </div>
    """
  end

  def sidebar_dropdown(assigns) do
    ~H"""
    <div class="px-3 mt-6 relative inline-block text-left">
      <div>
        <button
          id={@id}
          type="button"
          class="group w-full bg-gray-100 rounded-md px-3.5 py-2 text-sm text-left font-medium text-gray-700 hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-purple-500"
          phx-click={show_dropdown("##{@id}-dropdown")}
          data-active-class="bg-gray-100"
          aria-haspopup="true"
        >
          <span class="flex w-full justify-between items-center">
            <span class="flex min-w-0 items-center justify-between space-x-3">
              <img
                class="w-10 h-10 bg-gray-300 rounded-full flex-shrink-0"
                alt=""
                src={@current_user.avatar_url}
              />
              <span class="flex-1 flex flex-col min-w-0">
                <span class="text-gray-900 text-sm font-medium truncate">
                  <%= @current_user.name %>
                </span>
                <span class="text-gray-500 text-sm truncate">@<%= @current_user.username %></span>
              </span>
            </span>
            <svg
              class="flex-shrink-0 h-5 w-5 text-gray-400 group-hover:text-gray-500"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fill-rule="evenodd"
                d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                clip-rule="evenodd"
              />
            </svg>
          </span>
        </button>
      </div>
      <div
        id={"#{@id}-dropdown"}
        phx-click-away={hide_dropdown("##{@id}-dropdown")}
        class="hidden z-10 mx-3 origin-top absolute right-0 left-0 mt-1 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-200"
        role="menu"
        aria-labelledby={@id}
      >
        <div class="py-1" role="none">
          <.link navigate={profile_path(@current_user)}>View Profile</.link>
          <.link navigate={nil}>Settings</.link>
          <.link href={Routes.o_auth_callback_path(Endpoint, :sign_out)} method={:delete}>
            Sign out
          </.link>
        </div>
      </div>
    </div>
    """
  end

  def show_dropdown(to) do
    JS.show(
      to: to,
      transition:
        {"transition ease-out duration-200", "transform opacity-0 scale-95",
         "transform opacity-100 scale-100"}
    )
    |> JS.set_attribute({"aria-expanded", "true"}, to: to)
  end

  def hide_dropdown(to) do
    JS.hide(
      to: to,
      transition:
        {"transition ease-in duration-75", "transform opacity-100 scale-100",
         "transform opacity-0 scale-95"}
    )
    |> JS.remove_attribute("aria-expanded", to: to)
  end
end
