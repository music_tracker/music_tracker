defmodule MusicTrackerWeb.UnsplashUtils do
  def get_photo_url(photo_id, opts \\ %{}) do
    {"links", urls} =
      Unsplash.Photos.get(photo_id)
      |> Enum.find(fn x ->
        case x do
          {"links", _} -> true
          _ -> false
        end
      end)

    download_url = Map.get(urls, "download")
    str_opts = Enum.map(opts, fn {k, v} -> "#{k}=#{v}" end) |> Enum.join("&")

    download_url <> "&" <> str_opts
  end
end
