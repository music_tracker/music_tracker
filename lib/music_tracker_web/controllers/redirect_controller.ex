defmodule MusicTrackerWeb.RedirectController do
  use MusicTrackerWeb, :controller

  import MusicTrackerWeb.UserAuth, only: [fetch_current_user: 2]

  plug :fetch_current_user

  def redirect_authenticated(conn, _) do
    redirect(conn, to: Routes.live_path(conn, MusicTrackerWeb.DashboardLive))
  end
end
