defmodule MusicTrackerWeb.OAuthCallbackController do
  use MusicTrackerWeb, :controller
  alias MusicTracker.Accounts
  alias MusicTrackerWeb.UserAuth

  plug Ueberauth

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_flash(:error, "Failed to authenticate.")
    |> redirect(to: "/")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    case Accounts.find_or_create(auth) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User created successfully.")
        |> UserAuth.log_in_user(user)

      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> redirect(to: "/")
    end
  end

  def sign_out(conn, _params) do
    conn
    |> UserAuth.log_out_user()
  end
end
