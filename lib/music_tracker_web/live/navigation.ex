defmodule MusicTrackerWeb.Navigation do
  import Phoenix.LiveView

  alias MusicTrackerWeb.Accounts

  def on_mount(:default, _params, _session, socket) do
    {:cont,
     socket
     |> assign(active_tab: nil)}
  end
end
