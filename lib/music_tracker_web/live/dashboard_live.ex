defmodule MusicTrackerWeb.DashboardLive do
  use MusicTrackerWeb, :live_view
  alias MusicTracker.Repo.Songs
  alias MusicTracker.Repo.Channels
  alias MusicTracker.Repo.Model.SongMedia

  def render(assigns) do
    ~H"""
    <div class="min-h-screen">
      <div class="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
        <h2 class="text-lg leading-6 font-medium text-gray-900"><%= gettext("Overview") %></h2>
        <dl class="mt-5 grid grid-cols-1 gap-5 sm:grid-cols-3">
          <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
              <%= gettext("Total songs tracked") %>
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900">
              <%= MusicTracker.Cldr.Number.to_string!(@song_count) %>
            </dd>
          </div>

          <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
              <%= gettext("Total songs played") %>
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900">
              <%= MusicTracker.Cldr.Number.to_string!(@play_history_count) %>
            </dd>
          </div>

          <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
              <%= gettext("Channels tracked") %>
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900"><%= @channels_count %></dd>
          </div>
        </dl>
      </div>
      <h2 class="max-w-6xl mx-auto mt-8 px-4 text-lg leading-6 font-medium text-gray-900 sm:px-6 lg:px-8">
        <%= gettext("Recent play history (last updated at %{now})",
          now: MusicTracker.Cldr.DateTime.to_string!(DateTime.now!(@timezone))
        ) %>
      </h2>
      <div class="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex flex-col mt-2">
          <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
            <table class="min-w-full divide-y divide-gray-200">
              <thead>
                <tr>
                  <th
                    scope="col"
                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    <%= gettext("Title") %>
                  </th>
                  <th
                    scope="col"
                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    <%= gettext("Channel") %>
                  </th>
                  <th
                    scope="col"
                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    <%= gettext("Start play") %>
                  </th>
                  <th
                    scope="col"
                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    <%= gettext("End play") %>
                  </th>
                </tr>
              </thead>
              <tbody class="bg-white divide-y divide-gray-200">
                <%= for sh <- @history do %>
                  <tr class="bg-white">
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                      <div class="">
                        <a href="#" class="flex items-center">
                          <!--  group inline-flex space-x-2 truncate text-sm"> -->
                          <img src={sh.cover} class="flex-shrink-0 h-8 w-8 mr-2" />
                          <p class="text-gray-500 truncate group-hover:text-gray-900">
                            <%= sh.title %>
                          </p>
                        </a>
                      </div>
                    </td>
                    <td class="px-6 py-4 text-left whitespace-nowrap text-sm text-gray-500">
                      <p class="text-gray-500 truncate group-hover:text-gray-900">
                        <.link href={sh.channel_homepage}><%= sh.channel_name %></.link>
                      </p>
                    </td>
                    <td class="px-6 py-4 text-left whitespace-nowrap text-sm text-gray-500">
                      <time datetime="2020-07-11">
                        <%= MusicTracker.Cldr.DateTime.to_string!(
                          DateTime.shift_zone!(sh.start_time, @timezone)
                        ) %>
                      </time>
                    </td>
                    <td class="px-6 py-4 text-left whitespace-nowrap text-sm text-gray-500">
                      <time datetime="2020-07-11">
                        <%= MusicTracker.Cldr.DateTime.to_string!(
                          DateTime.shift_zone!(sh.end_time, @timezone)
                        ) %>
                      </time>
                    </td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    """
  end

  def mount(_params, _session, socket) do
    Songs.subscribe_trackers_activity()

    {:ok,
     assign(socket,
       live_title: gettext("Dashboard"),
       song_count: Songs.count_all_songs(),
       play_history_count: Songs.count_all_play_history(),
       channels_count: Channels.count_all_channels(),
       history: Songs.recent_play_history() |> to_table_date()
     )}
  end

  def handle_info({:song_analyzed, _}, socket) do
    {:noreply,
     assign(socket,
       song_count: Songs.count_all_songs(),
       play_history_count: Songs.count_all_play_history(),
       history: Songs.recent_play_history() |> to_table_date()
     )}
  end

  defp to_table_date(song_history) do
    song_history
    |> Enum.map(fn history ->
      cover =
        case history.song.medias |> List.first() do
          %SongMedia{type: "covers", payload: payload} -> Map.get(payload, "cover")
          %SongMedia{type: "itunes", payload: payload} -> Map.get(payload, "image")
          %SongMedia{type: "spotify", payload: payload} -> Map.get(payload, "image")
          %SongMedia{type: "deezze", payload: payload} -> Map.get(payload, "image")
          _ -> ""
        end

      %{
        title: history.song.title,
        channel_name: history.channel.name,
        channel_homepage: history.channel.homepage,
        start_time: history.start_time,
        end_time: history.end_time,
        duration: DateTime.diff(history.end_time, history.start_time),
        cover: cover
      }
    end)
  end
end
