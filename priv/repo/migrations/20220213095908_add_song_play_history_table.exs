defmodule MusicTracker.Repo.Migrations.AddSongPlayHistoryTable do
  use Ecto.Migration

  def change do
    create table(:song_play_history) do
      add :start_time, :utc_datetime
      add :end_time, :utc_datetime
      add :extract_hash, :string
      add :channel_id, references(:channel, on_delete: :delete_all), null: false
      add :song_id, references(:song, on_delete: :delete_all), null: false
      timestamps(updated_at: false)
    end

    create unique_index(:song_play_history, [:extract_hash])
  end
end
