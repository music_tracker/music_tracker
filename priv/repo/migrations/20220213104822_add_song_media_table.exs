defmodule MusicTracker.Repo.Migrations.AddSongMediaTable do
  use Ecto.Migration

  def change do
    create table(:song_media) do
      add :type, :string
      add :payload, :map
      add :song_id, references(:song, on_delete: :delete_all), null: false
      timestamps(updated_at: false)
    end
  end
end
