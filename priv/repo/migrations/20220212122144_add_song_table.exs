defmodule MusicTracker.Repo.Migrations.AddSongTable do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION pg_trgm"

    create table(:song) do
      add :title, :string
      add :album, :string
      add :label, :string
      add :year, :integer
      timestamps()
    end

    create index(:song, ["title gin_trgm_ops"], name: :song_title_trigram_index, using: :gin)
  end
end
